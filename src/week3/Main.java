package week3;

import java.io.*;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) throws ArithmeticException {
        /**
         * Variables
         */
        int loopCondition = 0; //condition for first do loop
        int chkNum = 0; //condition for second do loop
        int num1 = 0; //input for first number
        int num2 = 0; //input for second number
        double value; //value of division operation

        do { //do while loop to validate user cannot divide by zero
            do { //do while loop to validate user cannot input non-integers
                try { //try catch block

                    //ask for input for two numbers and save the as variables
                    Scanner input = new Scanner(System.in); //scanner object

                    //state purpose of program
                    System.out.println("This program takes two numbers and divides them.");
                    System.out.println("--------------------");

                    //ask for first number
                    System.out.println("Enter a number:");
                    System.out.print(">");

                    //validate user input to make sure they use integers
                    if (input.hasNextInt()) {
                        num1 = input.nextInt();
                        chkNum = 0;
                    }else{chkNum = 1;}
                    System.out.println("--------------------");

                    //ask for second number
                    System.out.println("Enter another number:");
                    System.out.print(">");

                    //validate user input to make sure they use integers
                    if (input.hasNextInt()) {
                        num2 = input.nextInt();
                        chkNum = 0;
                    }else{chkNum = 1;}
                    System.out.println("--------------------");

                    //divide the two numbers inputted by user
                    value = num1 / num2;
                    System.out.println("The value is " + value);
                    System.out.println("--------------------");
                    loopCondition = 1; //stop do loop
                    chkNum = 1; //stop do loop

                } catch (ArithmeticException e) {
                    System.out.println("What you have entered into the program cannot be executed.");
                    System.out.println("You CANNOT divide by zero.");
                    System.out.println("You CANNOT use letters or words.");
                    System.out.println("You must enter numbers");
                    System.out.println("--------------------");
                }
            } while (loopCondition == 0); //continue do loop
        }while (chkNum == 0); //continue do loop

      //indicate that program is now over
        System.out.println("Nice Dividing!");
        System.out.println("The program is now over.");
        
    }
}
